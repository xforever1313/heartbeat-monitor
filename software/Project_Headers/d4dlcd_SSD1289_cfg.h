/******************************************************************************
* 
* Copyright (c) 2009 Freescale Semiconductor;
* All Rights Reserved                       
*
*******************************************************************************
*
* THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR 
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  
* IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
* INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
* IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
* THE POSSIBILITY OF SUCH DAMAGE.
*
***************************************************************************//*!
*
* @file      d4dlcd_SSD1289_cfg.h
*
* @author    b01119
* 
* @version   0.0.1.0
* 
* @date      Mar-4-2011
* 
* @brief     D4D low level driver SSD1289 hardware level LCD header configuration file 
*
*******************************************************************************/

#ifndef __D4DLCD_SSD1289_CFG_H
#define __D4DLCD_SSD1289_CFG_H

  /******************************************************************************
  * includes
  ******************************************************************************/
  
  //#include "derivative.h"       /* include peripheral declarations and more for S08 and CV1 */
  //#include "support_common.h" /* include peripheral declarations and more for CV2 */


  /******************************************************************************
  * Constants
  ******************************************************************************/
  

#endif /* __D4DLCD_SSD1289_CFG_H */










