/******************************************************************************
* 
* Copyright (c) 2010 Freescale Semiconductor;
* All Rights Reserved                       
*
*******************************************************************************
*
* THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR 
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  
* IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
* INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
* IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
* THE POSSIBILITY OF SUCH DAMAGE.
*
***************************************************************************//*!
*
* @file      d4dlcdhw_common.h
*
* @author    b01119
* 
* @version   0.0.2.0
* 
* @date      Jun-28-2010
* 
* @brief     D4D driver - common low level driver header file 
*
*******************************************************************************/

#ifndef __D4DRVHW_COMMON_H
#define __D4DRVHW_COMMON_H

  /******************************************************************************
  * Includes
  ******************************************************************************/
  
  /******************************************************************************
  * Macros 
  ******************************************************************************/

  /******************************************************************************
  * Constants
  ******************************************************************************/

  /******************************************************************************
  * Types
  ******************************************************************************/

  /******************************************************************************
  * Macros 
  ******************************************************************************/

  /******************************************************************************
  * Global variables
  ******************************************************************************/
 
  /******************************************************************************
  * Global functions
  ******************************************************************************/
  void D4DLCD_Delay_ms_Common(unsigned short period);
  void D4DLCDHW_CommonInit(void);
#endif /* __D4DRVHW_COMMON_H */










