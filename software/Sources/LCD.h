/*
 * LCD.h
 *
 *  Created on: Nov 8, 2013
 *      Author: srh7240
 */

#ifndef LCD_H_
#define LCD_H_

#include "main.h"
#include "mcg.h"
#include "sim.h"
#include "pit.h"


// Initialize the TWR-LCD Screen
D4D_EXTERN_SCREEN(screen);
/**************************************************************//*!
 *
 * Global variables
 *
 ******************************************************************/

char screenText[40];

unsigned char divXAxis = 1; // Divide the X-axis by this value. 1 = every sample drawn. 2 = Every other sample drawn.

char textDrawn = 0;

TIME_FLAGS time;
LWord time100sm_cnt = 0;

unsigned char samples[SAMPLE_SIZE]; // Stored Samples
unsigned volatile short sampleWritePtr; // Index to put the next sample in samples
unsigned volatile short sampleCount; // Number of samples in samples. Maxes at SAMPLE_SIZE
unsigned short samplePtr; // Index to send the next sample wirelessly.

/**************************************************************//*!
 * Prototypes
 ******************************************************************/
static void pit_ch0_callback(void);
void MCU_Init(void);
void Timer_Init(void);
void addSample(unsigned char i);
/*****************************************************************/

/**************************************************************//*!
 * Init Clock and PLL
 ******************************************************************/
void MCU_Init(void) {
	SIM_Init(SIM_MODULE_CONFIG_LCD);
	PLL_Init(CORE_CLK_MHZ, REF_CLK);
}
/*****************************************************************/

/**************************************************************//*!
 *  Periodic Timer interrupt initialization - 25ms
 ******************************************************************/
#define PIT_PERIOD (D4D_MCU_BUS_CLOCK/1000)*25

void Timer_Init(void) {
	PIT_Init (PIT0,PIT_CH_CONFIG,PIT_PERIOD,1,pit_ch0_callback);
	PIT_Enable ();
}

/**************************************************************//*!
 * Periodic IRQ callback function
 ******************************************************************/
static void pit_ch0_callback(void) {
	static Byte actual_time = 0;
	TIME_FLAGS flags;

	actual_time++;
	flags.all = (D4D_BIT_FIELD) (actual_time ^ (actual_time - 1));

	time.all |= flags.all;

	if (flags.bits.b50ms) {
		//  Key_CheckKeys();
		//D4D_CheckTouchScreen();
	}

	if (flags.bits.b100ms) {
		time100sm_cnt++;
	}
}

/********************************************************************/

// This function adds a sample to a buffer.
// Call this function every time you want to draw a sample to the LCD screen (when divXAxis = 1).
void addSample(unsigned char i) {
	// Store only the upper 8-bits into the sample buffer.
	samples[sampleWritePtr] = i;

	// Increment the sample write position pointer.
	sampleWritePtr++;
	if (sampleWritePtr >= SAMPLE_SIZE) {
		sampleWritePtr = 0;
	}
	if ((sampleWritePtr % SAMPLE_SIZE) == samplePtr) {
		samplePtr = samplePtr % SAMPLE_SIZE;
	}
}

void setText(char *c) {
	char *txt = screenText;
	do {
		if(*txt != *c) {
			textDrawn = 1;
		}
		*txt++ = *c++;
	} while(*c != 0);
}

#endif /* LCD_H_ */
