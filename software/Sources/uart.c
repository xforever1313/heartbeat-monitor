/*
 * File:        uart.c
 * Purpose:     Provide UART routines for serial IO
 * Authors:     Seth Hendrick, Alex Sarra
 * Notes:
 *
 */

#include "derivative.h"
#include "uart.h"

/********************************************************************/
/*
 * Initialize the UART for 8N1 operation, 115200 Baud rate, interrupts
 * disabled, and no hardware flow-control
 *
 * System Clock is 50 MHz
 *
 */
void uart_init ()
{
	//set the clock for UART3
	SIM_SCGC4 = (1 << 13);
		
    // Set baud rate to 115200
    UART3_BDH = 0x00;
    UART3_BDL = 27;
    UART3_C4 = 0x04;
    
    //Set control registers
    UART3_C1 = 0; //?
    UART3_C2 = 0x0C; //FF
}

/********************************************************************/
/*
 * Wait for a character to be received on the specified UART
 *
 *
 * Return Values:
 *  the received character
 */
char uart_getchar ()
{
	while(uart_getchar_present() == 0);
	return UART3_D;
}

/********************************************************************/
/*
 * Wait for space in the UART Tx FIFO and then send a character
 *
 * Parameters:
 *  ch			 character to send
 */
void uart_putchar (char ch)
{
    while ((UART3_S1 & 0x80) == 0); /*Wait for space in buffer */
    UART3_D = ch;
}

/********************************************************************/
/*
 * Check to see if a character has been received
 *
 * Parameters:
 *  channel      UART channel to check for a character
 *
 * Return values:
 *  0       No character received
 *  1       Character has been received
 */
int uart_getchar_present ()
{
	int ret = 0;
	if ((UART3_S1 & 0x10) != 0){
		ret = 1;
	}
	return ret;
}
/********************************************************************/

