/*
 * Rochester Institute of Technology
 * Department of Computer Engineering
 * Interface and Digital Electronics
 *
 * Authors: Seth Hendrick, Alex Sarra
 *
 * Date: 11/18/2013
 *
 * This is the main program for the heart beat monitor
 * Compile with LCD defined (-DLCD for GCC) to enable the LCD driver
 * Do not comple with LCD defined to disable the LCD driver.
 *
 * Filename: main.c
 */

#define LCD

#include "derivative.h" /* include peripheral declarations */
#ifdef LCD
#include "LCD.h"
#endif
#include "mcg.h"
#include "uart.h"

typedef enum slope{
	DOWN,
	UP
}slope_t;

typedef enum mybool{
	T,
	F
}bool_t;

int ADCValue = 0;
int lastADCValue = 0;

int nextADCValue = 0;
int nextLastADCValue = 0;

int oneVoltValue = 1239;
int numberOfSamples = 0;
int voltageOffset = 10;

bool_t doneConverting;

slope_t calculateSlopeDirection(){
	slope_t ret = UP;
	int s = ADCValue - lastADCValue;
	if (s <= 0){
		ret = DOWN;
	}
	return ret;
}

bool_t isAtOneVolt(){
	bool_t ret = F;
    if ((lastADCValue < oneVoltValue) && (ADCValue >= oneVoltValue)){
    	ret = T;
    }
    else if ((lastADCValue >= oneVoltValue) && (ADCValue < oneVoltValue)){
    	ret = T;
    }
    return ret;
}

void printBPM(){
	int bpm = 60000/numberOfSamples;
    if ((bpm > 40) && (bpm < 300)){
	    char output[40];
	    sprintf(output, "Your BPM: %d\n\r", bpm);
	    io_printf(output);
    }
	numberOfSamples = 0;
}

void PDB_INIT(void) {
	//Enable PDB Clock
	SIM_SCGC6 |= SIM_SCGC6_PDB_MASK;
	PDB0_CNT = 0x0000;
	PDB0_MOD = 50000; // 50,000,000 / 50,000 = 1000
	PDB0_CH1DLY0 = 0;
	PDB0_SC = PDB_SC_PDBEN_MASK | PDB_SC_CONT_MASK | PDB_SC_TRGSEL(0xf)
			| PDB_SC_LDOK_MASK;
	PDB0_CH1C1 = PDB_C1_EN(0x01) | PDB_C1_TOS(0x01);
}

void DAC0_INIT(void) {
	//Enable DAC clock
    SIM_SCGC2 |= SIM_SCGC2_DAC0_MASK;
    DAC0_C0 = DAC_C0_DACEN_MASK | DAC_C0_DACRFS_MASK;
    DAC0_C1 = 0;
}

void ADC1_INIT(void) {
    unsigned int calib;

    // Turn on ADC0
    SIM_SCGC3 |= SIM_SCGC3_ADC1_MASK;

    // Configure CFG Registers
    // Configure ADC to divide 50 MHz down to 6.25 MHz AD Clock, 16-bit single ended
    ADC1_CFG1 = (0b01101100);
    
    
    // Do ADC Calibration for Singled Ended ADC. Do not touch.
    ADC1_SC3 = ADC_SC3_CAL_MASK;
    while ( (ADC1_SC3 & ADC_SC3_CAL_MASK) != 0 );
    calib = ADC1_CLP0;
    calib += ADC1_CLP1;
    calib += ADC1_CLP2;
    calib += ADC1_CLP3;
    calib += ADC1_CLP4;
    calib += ADC1_CLPS;
    calib = calib >> 1;
    calib |= 0x8000;
    ADC1_PG = calib;
    
	// Configure SC registers.
    // Select hardware trigger.
    ADC1_SC2 = 0b01000000;


    // Configure SC1A register.
    // Select ADC Channel and enable interrupts. Use ADC1 channel DAD3 (AN3, Pin A27) in single ended mode.
    ADC1_SC1A = 0b01000011;


	// Enable NVIC interrupt
	NVICISER1 |= NVIC_ISER_SETENA(1<<26);//ADC1 Interrupt
}

// ADC1 Conversion Complete ISR
void ADC1_IRQHandler(void) {
	// Read the result (upper 12-bits). This also clears the Conversion complete flag.
	unsigned short i = ADC1_RA >> 4;

	// Do stuff with the result here.
	nextLastADCValue = nextADCValue;
	nextADCValue = i;
	++numberOfSamples;
	
    //Set DAC output value (12bit)
    DAC0_DAT0L = (char)(i & 0x00FF);    //set low 8 bits
    DAC0_DAT0H = (char)(i >> 8);    //set high 4 bits
    doneConverting  = T;
}

void enable_uart(){
	// Enable UART Pins
	SIM_SCGC5 |= SIM_SCGC5_PORTC_MASK;
	PORTC_PCR16 = PORT_PCR_MUX(0x03);
	PORTC_PCR17 = PORT_PCR_MUX(0x03);

    uart_init();    
}

int main(void) {
	int counter = 0;
	bool_t alreadyPrinted = F; /*Prevents printing twice when going up*/    
	doneConverting = F;
	
    #ifndef LCD
	MCG_BLPE();
    #else
    MCU_Init();
    Timer_Init();
    
	// Initialize TWR-LCD Screen. DO NOT REMOVE
	D4D_Init(&screen);
	D4D_SetOrientation(D4D_ORIENT_LANDSCAPE);
    
	sampleWritePtr = 0;
	samplePtr = 0;
    
    #endif
    
    enable_uart();
    DAC0_INIT();
    ADC1_INIT();
    PDB_INIT();

	// Start the PDB (ADC Conversions)
	PDB0_SC |= PDB_SC_SWTRIG_MASK;

	io_printf("Alex and Seth's Heartbeat monitor!\n\r");
	   
    for(;;) {
    	if ((alreadyPrinted == F) && (isAtOneVolt() == T) && (calculateSlopeDirection() == UP)){
    	    printBPM();
    	    alreadyPrinted = T;
    	}
    	else if ((alreadyPrinted == T) && (isAtOneVolt() == T) && (calculateSlopeDirection() == DOWN)){
    		alreadyPrinted = F;
    	}

        #ifdef LCD
		if(time.bits.b25ms) {
			setText("Your Heartbeat");
			// Add a sample to draw. divXAxis = 1 so each sample = 1 pixel.
			addSample((ADCValue >> 6) * 5);
		}

		// Run the TWR-LCD Routine. DO NOT REMOVE
		if(time.bits.b25ms) {
			D4D_Poll(); // D4D poll loop
		}
        #endif
		
		while (doneConverting == F){};
		ADCValue = nextADCValue;
		lastADCValue = nextLastADCValue;
		doneConverting = F;

    }

    return 0;
}
