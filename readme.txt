Software:
    Code for the heartbeat monitor is located in the "software" folder.  Freescale's codewarrior IDE is needed to use, along with a Freescale K60 tower in order to run the code.
    
IDELab7HeartbeatMonitor is the lab report using the code as a heartbeat monitor.  Schematics are inside.

IDELab8EKG is the lab report using the code as an EKG.  Schematics are inside.